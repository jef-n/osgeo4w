#!/bin/bash

from=rsync://download.osgeo.org/download/osgeo4w
to=~/public_html/osgeo4w

echo -e "Content-Type: text/plain\r\n\r"

cd $to

if ! lockfile -r0 mirroring; then
        echo Mirror in progress since $(date -r mirroring)
        exit 0
fi

echo "Starting sync..."

(
        trap "rm -f $PWD/mirroring" EXIT

        exec >>/tmp/osgeo4w-mirror.log 2>&1

        rm -f /tmp/osgeo4w-files

        for a in x86 x86_64; do
                mkdir -p $a
                echo "$(date): Downloading $a/setup.ini."
                rsync $from/$a/setup.ini.bz2 $a/setup.ini.bz2.syncing
                bzcat $a/setup.ini.bz2 >$a/setup.ini.syncing

                perl -ne 'print "/$1\n" if /^(?:install|source|license): (\S+) .*$/;' $a/setup.ini.syncing >>/tmp/osgeo4w-files
        done

        rsync -v --inplace -a --stats --files-from=/tmp/osgeo4w-files $from/ ./

        for a in x86 x86_64; do
                mv $a/setup.ini.bz2.syncing $a/setup.ini.bz2
                mv $a/setup.ini.syncing $a/setup.ini
                echo /$a/setup.ini >>/tmp/osgeo4w-files
        done

        echo /verify.sh >>/tmp/osgeo4w-files
        echo /mirroring >>/tmp/osgeo4w-files

        echo "$(date): Deleting obsolete files [$?]"

        find . -type f | sed -e "s/^\.//" | fgrep -v -f /tmp/osgeo4w-files | sed -e "s/^/\./" | xargs echo rm -v

        echo "$(date): Syncing done [$?]"
) &
